import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StudentsComponent } from './students/students.component';
import { providerDef } from '@angular/core/src/view';
import { StudentService } from './service/student-service';
import { StudentDataImplService } from './service/student-data-impl.service';
import { StundentInfService } from './service/stundent-inf.service';

@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    {provide: StudentService,
      useClass: StundentInfService,

    },
    {provide: StudentService,
      useClass: StudentDataImplService,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
