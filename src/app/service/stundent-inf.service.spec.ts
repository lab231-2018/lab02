import { TestBed, inject } from '@angular/core/testing';

import { StundentInfService } from './stundent-inf.service';

describe('StundentInfService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StundentInfService]
    });
  });

  it('should be created', inject([StundentInfService], (service: StundentInfService) => {
    expect(service).toBeTruthy();
  }));
});
