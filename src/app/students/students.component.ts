import { Component, OnInit } from '@angular/core';
import { Student } from '../entity/student';
import { StudentDataImplService } from '../service/student-data-impl.service';
import { StundentInfService } from '../service/stundent-inf.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
    name = 'SE331';
    students:Student[] ;
  
    averageGpa(): number {
      let sum = 0;
      for (let student of this.students) {
        sum += student.gpa;
      }
      return sum / this.students.length;
  
    }
  
  constructor(private studentService: StudentDataImplService, private studentService2: StundentInfService) { }

  ngOnInit() {
    this.studentService.getStudents().subscribe(
      students => {
        this.students = students;
      }
    )
    this.studentService2.getStudents().subscribe(
      students => {
        this.students = students;
      }
    )
  }

}


